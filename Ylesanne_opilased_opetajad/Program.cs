﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp20
{
    class Program
    {

        static string failInimene = @"..\..\Data\Inimesed.txt";
        static string failOpilane = @"..\..\Data\Opilased.txt";
        static string failOpetaja = @"..\..\Data\Opetjad.txt";
        static string failOppeaine = @"..\..\Data\Ained.txt";
        static string failKlassinimi = @"..\..\Data\Klassid.txt";

        static void Main(string[] args)
        {
            LoeInimesed();

            string küsimus = "";
            do
            {
                Console.Write("mida teha tahad\nList - loetelu, Lisa - uus inimene, Exit - lõpeta: ");
                küsimus = Console.ReadLine().ToLower();
                switch (küsimus)
                {
                    case "list":
                        Console.WriteLine("Inimesed: ");
                        foreach (var x in Inimene.Inimesed)
                        {
                            Console.WriteLine(x);
                        }
                        Console.WriteLine();
                        if (Opilane.Opilased.Count > 0)
                        { 
                            Console.WriteLine("Opilased: ");
                            foreach (var x in Opilane.Opilased)
                            {
                            Console.WriteLine(x);
                            }
                        }
                        Console.WriteLine();

                        if (Opetaja.Opetajad.Count > 0)
                        {
                            Console.WriteLine("Õpetajad: ");
                            foreach (var x in Opetaja.Opetajad)
                            {
                                Console.WriteLine(x);
                            }
                        }
                        Console.WriteLine();

                        if (Oppeaine.Oppeained.Count > 0)
                        {
                            Console.WriteLine("Õppeained: ");
                            foreach (var x in Oppeaine.Oppeained)
                            {
                                Console.WriteLine(x);
                            }
                        }
                        Console.WriteLine();

                        if (Klass.Klassid.Count > 0)
                        {
                            Console.WriteLine("Klassid: ");
                            foreach (var x in Klass.Klassid)
                            {
                                Console.WriteLine(x);
                            }
                        }



                        break;
                    case "lisa":

                        Console.Write("Anna nimi: ");
                        string nimi = Console.ReadLine();
                        Console.Write("Anna vanus: ");
                        int vanus = int.Parse(Console.ReadLine());
                        Inimene.Inimesed.Add(new Inimene() { Nimi = nimi, Vanus = vanus });
                        Console.WriteLine("Kas lisasid õpilase(1) või õpetaja(2): ");
                        int opilaneOpetja = int.Parse(Console.ReadLine());
                        if (opilaneOpetja == 1)
                        {
                            Console.WriteLine("Mis klassis õpilane õpib: ");
                            string klassinimi = Console.ReadLine();
                            Opilane.Opilased.Add(new Opilane() { Nimi = nimi, Vanus = vanus, KlassiNimi = klassinimi });
                            Klass.Klassid.Add(new Klass() { KlassiNimi = klassinimi });











                            //Klass.Klassid.Add(new Klass() { KlassiNimi = klassinimi });




                        }
                        else if (opilaneOpetja == 2)
                        {
                            Console.WriteLine("Mis ainet õpetab: ");
                            string aineNimi = Console.ReadLine();
                            Opetaja.Opetajad.Add(new Opetaja() { Nimi = nimi, Vanus = vanus, AineNimi = aineNimi });
                            Oppeaine.Oppeained.Add(new Oppeaine() { AineNimi = aineNimi });
                        }
                        else
                            Console.WriteLine("Ei ole Õpilane ega Õpetaja");
                        

                        break;
                    case "exit":
                    case "":
                        Console.WriteLine("no ma siis lõpetan");
                        break;
                    default:
                        break;

                }

            }
            while (küsimus != "" && küsimus != "exit");

            SalvestaInimesed();

            

            //pilane jaak = new Opilane("Jaak", 12, k1b);
            
            foreach (var x in Klass.Klassid)
            Console.WriteLine(x);


            
        }

        static void LoeInimesed()
        {
            string[] loetudRead = File.ReadAllLines(failInimene);
            foreach (var r in loetudRead)
                Inimene.Inimesed.Add(new Inimene() { Nimi = r.Split(' ')[0], Vanus = int.Parse(r.Split(' ')[1]) });

            string[] loetudRead1 = File.ReadAllLines(failOpilane);
            foreach (var r in loetudRead1)
                Opilane.Opilased.Add(new Opilane() { Nimi = r.Split(' ')[0], Vanus = int.Parse(r.Split(' ')[1]), KlassiNimi = r.Split(' ')[2]});

            string[] loetudRead2 = File.ReadAllLines(failOpetaja);
            foreach (var r in loetudRead2)
                Opetaja.Opetajad.Add(new Opetaja() { Nimi = r.Split(' ')[0], Vanus = int.Parse(r.Split(' ')[1]), AineNimi = r.Split(' ')[2]});

            string[] loetudRead3 = File.ReadAllLines(failOppeaine);
            foreach (var r in loetudRead3)
                Oppeaine.Oppeained.Add(new Oppeaine(){AineNimi=r.Split()[0]});
            string[] loetudRead4 = File.ReadAllLines(failKlassinimi);
            foreach (var r in loetudRead4)
                Klass.Klassid.Add(new Klass() {KlassiNimi = r.Split()[0]});
        }

        static void SalvestaInimesed()
        {
            File.WriteAllLines(failInimene,
            Inimene.Inimesed.Select(x => x.ToString()).ToArray()
            );
            File.WriteAllLines(failOpetaja,
            Opetaja.Opetajad.Select(x => x.ToString()).ToArray()
            );
            File.WriteAllLines(failOpilane,
            Opilane.Opilased.Select(x => x.ToString()).ToArray()
            );
            File.WriteAllLines(failOppeaine,
            Oppeaine.Oppeained.Select(x => x.ToString()).ToArray()
            );
            File.WriteAllLines(failKlassinimi,
            Klass.Klassid.Select(x => x.ToString()).ToArray()
            );
        }

        
        
        
    }

    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();
        public string Nimi;
        public int Vanus;
        

        //public Inimene() : this("", 0) { }

        //public Inimene (string nimi, int vanus) { Nimi = nimi; Vanus = vanus; }

        public override string ToString()
        {
            return $"{Nimi} {Vanus}";
        }
    }

    class Opilane : Inimene
    {
        public static List<Opilane> Opilased = new List<Opilane>();

        //public Klass MisKlassis = null;
        public string KlassiNimi = ""; // siia klassi nimi, kus õpib

        //public Opilane() : base("", 0) { }

        //public Opilane(string nimi, int vanus, string klassinimi) : base(nimi, vanus) { Opilased.Add(this); }

        //public Opilane(string nimi, int vanus) : base(nimi, vanus) { }


        //{ this.MisKlassis = klassinimi; klassinimi.Opilased.Add(this); }

        //public override string ToString()  // - hetkel ei ole vaja, muidu kirjutab faili üle kujule, mida ei oska lugeda
        //{
        //    return $" { MisKlassis?.KlassiNimi } õpilane {Nimi} vanus: {Vanus}";
        //}

        //Mis klassis käib - sellega tulevad ained, õpetajad
        public override string ToString()
        {
            return $"{Nimi} {Vanus} {KlassiNimi}";
        }

    }

    class Opetaja : Inimene
    {
        public static List<Opetaja> Opetajad = new List<Opetaja>();
        // Õppeaine
        // $" {Nimi} on {õppeaine} õpetja"

        public string AineNimi; // siia aine, mida õpetab

        public override string ToString()
        {
            return $"{Nimi} {Vanus} {AineNimi}";
        }
    }

    class Klass:Opilane
    {
        public static List<Klass> Klassid = new List<Klass>();
        //internal List<Opilane> Opilased = new List<Opilane>();
        //public string KlassiNimi;

        //public Klass(string klassinimi) { KlassiNimi = klassinimi; Klassid.Add(this); }



        public override string ToString()
        {
            return $"{KlassiNimi}";




        }
        // mitmes klass
        // sellega kaasnevad õppeained
    }

    class Oppeaine:Opetaja
    {
       
        public static List<Oppeaine> Oppeained = new List<Oppeaine>();
        
        public override string ToString()
        {
            return $"{AineNimi}";
        }


        //vaja kasutada: klassis, õpetajatel, õpilastel, hinnetes

    }

    public enum Hinne {puudub, puudulik, rahuldav, hea, vägahea, suurepärane}
}

